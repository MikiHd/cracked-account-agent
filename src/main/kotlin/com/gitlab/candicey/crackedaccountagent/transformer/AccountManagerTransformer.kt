package com.gitlab.candicey.crackedaccountagent.transformer

import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.*
import java.lang.instrument.ClassFileTransformer
import java.security.ProtectionDomain

object AccountManagerTransformer : ClassFileTransformer {
    override fun transform(
        loader: ClassLoader,
        className: String,
        classBeingRedefined: Class<*>?,
        protectionDomain: ProtectionDomain?,
        classfileBuffer: ByteArray?
    ): ByteArray? {
        if (!className.startsWith("com/moonsworth/lunar/")) {
            return null
        }

        val classNode = ClassNode()
        val classReader = ClassReader(classfileBuffer)
        classReader.accept(classNode, 0)

        val methods = classNode.methods

        var foundMethodNode: MethodNode? = null

        methods.forEach { methodNode ->
            if (methodNode.desc == "(Ljava/lang/Object;[Ljava/lang/Object;)V") {
                val insnList = methodNode.instructions
                val accountsLdc = insnList
                    .filterIsInstance<LdcInsnNode>()
                    .find { it.cst == "Accounts" }
                    ?: return@forEach

                val next = insnList.get(insnList.indexOf(accountsLdc) + 1) as? VarInsnNode ?: return@forEach
                if (next.opcode != Opcodes.ALOAD || next.`var` != 0) {
                    return@forEach
                }

                val nextNext = insnList.get(insnList.indexOf(next) + 1) as? VarInsnNode ?: return@forEach
                if (nextNext.opcode != Opcodes.ALOAD || nextNext.`var` != 1) {
                    return@forEach
                }

                foundMethodNode = methodNode
            }
        }

        if (foundMethodNode == null) {
            return null
        }

        val previousMethodNode = methods[methods.indexOf(foundMethodNode) - 1]
        val previousInsnList = previousMethodNode.instructions
        previousInsnList.clear()
        previousInsnList.add(InsnNode(Opcodes.ICONST_1))
        previousInsnList.add(InsnNode(Opcodes.IRETURN))

        println("[CrackedAccountAgent] Injected code into ${classNode.name}#${previousMethodNode.name}")

        val classWriter = ClassWriter(ClassWriter.COMPUTE_MAXS)
        classNode.accept(classWriter)

        return classWriter.toByteArray()
    }
}